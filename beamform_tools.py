import numpy as np
from scipy.signal import butter, filtfilt
import matplotlib.pyplot as plt


if __name__ == "__main__":
    print('Error: Module run as script')   

## Set up spectra
def get_spectra(waveforms, plot=True):
    """
    ff,spectra = get_spectra(waveforms)
    OBSPY has some nice spectra functions, but we'll define things manually here for fun.
    """

    # nfft will be a power of 2.
    # the number of points returned by rfft is only half (+1) of this
    nsta = np.shape(waveforms)[0]
    npts = waveforms[0].stats.npts
    nfft = (2.0**np.round(np.log2(npts))).astype(int)
    nsave = np.int((nfft/2)+1)

    # Take fourier transforms of all our traces.
    delta = waveforms[0].stats.delta # timestep between regularly spaced samples.
    df=1.0/(2*nfft*delta)
    ff = np.linspace(0.0, 1.0/(2.0*delta), nsave)
    spectra = np.zeros([np.size(ff),nsta],dtype='complex')
    for ii,tr in enumerate(waveforms):
        spectra[:,ii] = np.fft.rfft(tr[:],n=nfft)

    if(plot):
        plt.semilogy(ff, np.abs(spectra))
        #plt.xlim([1e-1,200])
        plt.xlim([0.5, 50])
        plt.ylim([10^-9,10^-3])
        plt.xlabel('Freq [Hz]')
        plt.ylabel('FFT output [units?]')
        plt.show()

    return ff, spectra


def plot_beam(P, ux, uy, title="Beamform",save=0,savename='none',cmax=0,cmin=None):
    dux = ux[1]-ux[0]
    duy = uy[1]-uy[0]

    vR = 2758.0/1000.0          # Expected velocity in km/s
    fig = plt.figure(figsize=(10,10))
    ax = fig.add_axes([0.1,0.1,0.6,0.6])  #x0,y0,dx,dy
    cmap = plt.get_cmap('inferno')
    i = plt.pcolor(ux-dux/2,uy-duy/2,np.abs(P.T),cmap=cmap,rasterized=True,shading='auto')#,vmin=-4,vmax=4)
    if(cmax==0):
        cmax=np.max(P)
    if(cmin==None):
        cmin=-cmax
    plt.clim(cmin,cmax)
    #plt.clim(-cmax,cmax)

    plt.axis('equal')
    plt.axis('tight')
    plt.xlim(min(ux)+dux,max(ux)-dux)
    plt.ylim(min(uy)+duy,max(uy)-duy)
    plt.xlabel('Slowness East-West [s/km]')
    plt.ylabel('Slowness North-South [s/km]')
    ax.tick_params(top=True,right=True)
    plt.plot([np.min(ux), np.max(ux)],[0,0],'w')
    plt.plot([0,0],[np.min(uy), np.max(uy)],'w')
    plt.title(title)
    
    sux = -1/vR
    suy = 0
    #plt.plot(sux,suy,'r*',markersize=20)
    theta = np.linspace(0,2*np.pi,100)
    sxx = np.cos(theta)*1/vR
    syy = np.sin(theta)*1/vR
    plt.plot(sxx,syy,'w--')
    
    colorbar_ax = fig.add_axes([0.75, 0.1, 0.03, 0.6])  #x0,y0,dx,dy
    fig.colorbar(i, cax=colorbar_ax)

    if(save==1):
        plt.savefig(savename, bbox_inches='tight')
    plt.show()



def stack_freqs(P):
    nf,nx,ny = np.shape(P)

    # check for nans
    P2 = P.copy()
    for ip in range(nf):
        P2[ip,np.isnan(P2[ip,:,:])] = 0

    # Stack normally
    P2_stack = np.abs(np.sum(P2,axis=0))/nf

    # Normalize each frequency before adding (spectral whitening)
    P2_norm=np.zeros_like(P)
    for ip in range(nf):
        P2_norm[ip,:,:] = P2[ip,:,:] / np.max(np.abs(P2[ip,:,:]))
    P2_norm = np.abs(np.sum(P2_norm,axis=0))/nf

    return(P2_stack,P2_norm)





