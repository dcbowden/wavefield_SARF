import numpy as np
from random import seed
from random import random
from scipy.signal import butter, filtfilt
import matplotlib.pyplot as plt



if __name__ == "__main__":
    print('Error: Module run as script')   

# Freq domain style random-signal

def make_cosine_taper(npts,taper_width, win_start=int(0),win_end=int(0)):
    # taper edges, inspired by obspy source code
    # Define a window, mostly ones, but with a cosine that starts and ends idx1:idx2, idx3:idx4
    idx1=win_start
    idx2=int(idx1+taper_width)
    if(win_end==0):
        idx4=npts
    else:
        idx4=win_end
    idx3=int(idx4-taper_width)
    #print(idx1,idx2,idx3,idx4)

    cos_win = np.zeros(npts,)
    cos_win[idx1:idx2 + 1] = 0.5 * (
        1.0 - np.cos((np.pi * (np.arange(idx1, idx2 + 1) - float(idx1)) /
                      (idx2 - idx1))))
    cos_win[idx2 + 1:idx3] = 1.0
    cos_win[idx3-1:idx4 ] = 0.5 * (
        1.0 + np.cos((np.pi * (float(idx3) - np.arange(idx3, idx4 + 1 )) /
                      (idx4 - idx3))))
    return cos_win


def make_random_seis(starttime,endtime,dt,f0,f1,plot=0,this_seed=1):

    time = np.linspace(starttime, endtime, int((endtime-starttime+dt)/dt))
    npts = np.size(time)
    
    # Define frequency spectrum of target signal
    nfft = (2.0**np.round(np.log2(npts))).astype(int)
    df=1.0/(nfft*dt)
    f=np.arange(0.0,1.0/dt,df)
    
    # Define a random spectrum
    seed(this_seed)
    phase=2.0*np.pi*(np.random.rand(nfft,)-0.5)
    # Consider not having random amplitude - defn of white noise?
    amp=np.random.rand(nfft,)
    spect = np.zeros([nfft,],dtype=complex)
    spect = amp*np.exp(1j*phase)
    
    # Filter (Define a window)
    i_f0 = (np.abs(f - f0)).argmin()
    i_f1 = (np.abs(f - f1)).argmin()
    taper_width = int((i_f1-i_f0)*.1)
    filtwin = make_cosine_taper(nfft,taper_width,win_start=i_f0,win_end=i_f1)
    spect = spect*filtwin
    
    # IFFT gives us our time domain signal
    signal = np.real(np.fft.irfft(spect,n=nfft))/dt
    signal = signal[0:npts]
    
    # taper in time domain
    taper_width = 1.0/dt/f0 # make the width at least as long as longest wavelength
    cos_win = make_cosine_taper(npts,taper_width,win_start=0,win_end=npts)
    signal=signal*cos_win

    # Normalize
    signal = signal/np.max(signal)

    if(plot==1):
        # Check
        # plt.plot(f,np.real(spect))
        # plt.plot(f,filtwin)
        # plt.xlim([0, f1+10])
        # plt.xlabel('Freq')
        # plt.title('Random Spectrum')
        # plt.show()

        plt.plot(time,signal)
        plt.xlabel('Time (seconds)')
        plt.ylabel('Amplitude')
        plt.show()
        
        spec2 = np.fft.fft(signal,n=nfft)
        plt.plot(1/f,np.real(spec2))
        plt.xlim([0,80])
        plt.xlabel('Period')
        plt.title('Random Spectrum after cutting and tapering')
        plt.show()

    return signal
